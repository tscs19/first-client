package com.alro.cloud.firstclient.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.MessageFormat;
import java.util.Date;

@RestController
@RequestMapping("${controller.client}")
public class ClientController {

    @GetMapping("/info")
    public ResponseEntity<String> generateName() {
        String template = "{0}  INFO  Client-1 with id = {1}";
        return ResponseEntity.ok(MessageFormat.format(template, new Date(), Math.random()));
    }
}
