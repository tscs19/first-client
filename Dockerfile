# Start with a base image containing Java runtime
FROM openjdk:8-jdk-alpine
LABEL maintainer="Aliaksandr Rahavoi <rogovoi200023507@gmail.com>"
# Add a volume pointing to /tmp
VOLUME /tmp
# Make port available to the world outside this container
EXPOSE 8000
# The application's jar file
ARG JAR_FILE=target/first-client-0.0.1-SNAPSHOT.jar
# Add the application's jar to the container
ADD ${JAR_FILE} first-client.jar
ENV JAVA_OPTS="-Xms128m -Xmx128m"
ENV SPRING_PROFILES_ACTIVE=docker
# Run the jar file
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/first-client.jar"]